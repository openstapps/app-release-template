# This file contains the fastlane.tools configuration for iOS
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

require 'json'
require 'base64'

default_platform(:ios)

current_build_number = 1
package_json = JSON.parse(File.read('../../../package.json'))

api_key = app_store_connect_api_key(
  key_id: ENV['APPLE_API_KEY_ID'],
  issuer_id: ENV['APPLE_API_KEY_ISSUER_ID'],
  key_content: ENV['APPLE_API_KEY_CONTENT'],
  is_key_content_base64: true,
  in_house: false
)

platform :ios do

  lane :fetch_highest_build_number do
    build_number_candidates = [1]
    build_number_candidates << latest_testflight_build_number(
        version: ENV['APP_MARKETING_VERSION'],
        initial_build_number: 1,
        app_identifier: ENV['IOS_BUNDLE_IDENTIFIER'],
        api_key: api_key
    )
    build_number_candidates << app_store_build_number(
      version: ENV['APP_MARKETING_VERSION'],
      initial_build_number: 1,
      app_identifier: ENV['IOS_BUNDLE_IDENTIFIER'],
      api_key: api_key
    )
    current_build_number = build_number_candidates.max
  end

  lane :configure do
    set_info_plist_value(
      path: "App/Info.plist",
      key: "NSLocationAlwaysAndWhenInUseUsageDescription",
      value: ENV['LOCATION_USAGE_DESCRIPTION']
    )
    set_info_plist_value(
      path: "App/Info.plist",
      key: "NSLocationWhenInUseUsageDescription",
      value: ENV['LOCATION_USAGE_DESCRIPTION']
    )
    set_info_plist_value(
      path: "App/Info.plist",
      key: "NSCalendarsFullAccessUsageDescription",
      value: ENV['CALENDAR_USAGE_DESCRIPTION']
    )
    set_info_plist_value(
      path: "App/Info.plist",
      key: "NSCalendarsUsageDescription",
      value: ENV['CALENDAR_USAGE_DESCRIPTION']
    )
    update_url_schemes(
      path: "App/Info.plist",
      url_schemes: [ENV['APP_URL_SCHEME']]
    )
    update_code_signing_settings(
      use_automatic_signing: true,
      path: "App.xcodeproj",                                         
      team_id: ENV['TEAM_ID'],               
      bundle_identifier: ENV['IOS_BUNDLE_IDENTIFIER'], 
      entitlements_file_path: "App/App.entitlements"
    )
    increment_version_number(
      version_number: ENV['APP_MARKETING_VERSION'], 
      xcodeproj: "App.xcodeproj"
    )
    fetch_highest_build_number
    increment_build_number(
      build_number: current_build_number + 1,
      xcodeproj: "App.xcodeproj"
    )
  end

  lane :build do
    build_app(workspace: "App.xcworkspace", scheme: "App")
  end

  desc "Submit a new version to iOS App Store"
  lane :release do
    configure
    build
    upload_to_app_store(
      skip_metadata: true,
      skip_screenshots: true,
      precheck_include_in_app_purchases: false,
      api_key: api_key
    )
  end

  desc "Submit a new version to Testflight"
  lane :beta do
    configure
    build
    upload_to_testflight(
      skip_submission: true,
      api_key: api_key
    ) 
  end
end