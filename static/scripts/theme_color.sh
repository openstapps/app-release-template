#!/usr/bin/env sh

if [ "$1" = "background" ]; then
  color=$(grep -oE "@include ion-$1-color\(.*"  "customizable/theme/colors.scss" | grep -oE "((, )?#[a-fA-F0-9]{3,6})+")
else
  color=$(grep -oE "@include ion-color\($1.*" "customizable/theme/colors.scss" | grep -oE "(, #[a-fA-F0-9]{3,6})+")
fi

if [ "$2" = "dark" ]; then
  echo "$color" | grep -oE "#[a-fA-F0-9]{3,6}$"
else
  echo "$color" | grep -oE "(^, |^)#[a-fA-F0-9]{3,6}" | grep -oE "#[a-fA-F0-9]{3,6}"
fi
