#!/usr/bin/env sh

. $PWD/app.conf

DEFAULT_APP_URL_SCHEME="de.anyschool.app"
APP_URL_SCHEME="${APP_URL_SCHEME:-$DEFAULT_APP_URL_SCHEME}"

ANDROID_MANIFEST_PATH="monorepo/frontend/app/android/app/src/main/AndroidManifest.xml"
ANDROID_STRINGS_PATH="monorepo/frontend/app/android/app/src/main/res/values/strings.xml"
ANDROID_COLORS_PATH="monorepo/frontend/app/android/app/src/main/res/values/colors.xml"
ANDROID_COLORS_NIGHT_PATH="monorepo/frontend/app/android/app/src/main/res/values-night/colors.xml"


git -C monorepo checkout -- frontend/app/android/build.gradle
git -C monorepo checkout -- frontend/app/android/gradle/wrapper/gradle-wrapper.properties
git -C monorepo checkout -- frontend/app/android/app/src/main/AndroidManifest.xml
git -C monorepo checkout -- frontend/app/android/app/src/main/res/values/styles.xml
git -C monorepo checkout -- frontend/app/android/app/src/main/res/values/colors.xml
git -C monorepo checkout -- frontend/app/android/app/src/main/res/values-night/colors.xml

# AndroidManifest.xml
xmlstarlet edit --pf --inplace \
  --update "/manifest/@package" --value "$ANDROID_PACKAGE_NAME" \
  $ANDROID_MANIFEST_PATH

xmlstarlet edit --pf --inplace \
  --update "/manifest/application/activity/@android:name" --value "$ANDROID_PACKAGE_NAME".MainActivity \
  $ANDROID_MANIFEST_PATH

# strings.xml
xmlstarlet edit --pf --inplace \
  --update "/resources/string[@name='package_name']" --value "$ANDROID_PACKAGE_NAME" \
  $ANDROID_STRINGS_PATH

xmlstarlet edit --pf --inplace \
  --update "/resources/string[@name='custom_url_scheme']" --value "$APP_URL_SCHEME" \
  $ANDROID_STRINGS_PATH

xmlstarlet edit --inplace \
  --delete "/resources/string[@name='app_host']" \
  --append "/resources/string[@name='custom_url_scheme']" --type elem -n string --value "$APP_LINK_HOST" \
  --insert "/resources/string[not(@name)]" --type attr -n name --value "app_host" \
  $ANDROID_STRINGS_PATH

xmlstarlet edit --inplace \
  --update "/resources/color[@name='colorPrimary']" --value "$(static/scripts/theme_color.sh primary)" \
  $ANDROID_COLORS_PATH

xmlstarlet edit --inplace \
  --update "/resources/color[@name='colorPrimaryDark']" --value "$(static/scripts/theme_color.sh primary)" \
  $ANDROID_COLORS_PATH

xmlstarlet edit --inplace \
  --update "/resources/color[@name='colorBackground']" --value "$(static/scripts/theme_color.sh background)" \
  $ANDROID_COLORS_PATH

xmlstarlet edit --inplace \
  --update "/resources/color[@name='colorBackground']" --value "$(static/scripts/theme_color.sh background dark)" \
  $ANDROID_COLORS_NIGHT_PATH
