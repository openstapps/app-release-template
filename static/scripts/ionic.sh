#!/usr/bin/env sh

. $PWD/app.conf

DEFAULT_APP_ID="de.anyschool.app"
DEFAULT_CONFIG_MODE="WEB"

APP_ID="${APP_ID:-$DEFAULT_APP_ID}"
APP_VERSION="${APP_MARKETING_VERSION:-$(jq '.version' monorepo/frontend/app/package.json)}"
CONFIG_MODE="${CONFIG_MODE:-$DEFAULT_CONFIG_MODE}"

if [ "$CONFIG_MODE" = 'ANDROID' ]; then
  APP_ID="$ANDROID_PACKAGE_NAME"
fi

if [ "$CONFIG_MODE" = 'IOS' ]; then
  APP_ID="$IOS_BUNDLE_IDENTIFIER"
fi

# ionic config
cat monorepo/frontend/app/ionic.config.json | jq '.name = $newName' --arg newName "$APP_NAME" > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/ionic.config.json
sed -E -i.bak "s|<title>StApps|<title>$APP_DISPLAY_NAME|g" monorepo/frontend/app/src/index.html

ROOT_THEME_DEFINITONS=$(cat monorepo/frontend/app/src/theme/colors.scss | grep -o :root | wc -l)

if [ $ROOT_THEME_DEFINITONS -gt 1 ]; then
  #update
  echo "SCSS theme has already been set"
else
  #insert
  sed -E -i.bak '/include|import/d' monorepo/frontend/app/src/theme/colors.scss
  printf '%s\n%s\n'  "$(cat customizable/theme/colors.scss)" "$(cat monorepo/frontend/app/src/theme/colors.scss)" > monorepo/frontend/app/src/theme/colors.scss
fi



# capacitor config
awk "/appName:.*,/ && !done { gsub(/appName:.*,/, \"appName: '$APP_NAME',\"); done=1}; 1" monorepo/frontend/app/capacitor.config.ts > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/capacitor.config.ts
awk "/appId:.*,/ && !done { gsub(/appId:.*,/, \"appId: '$APP_ID',\"); done=1}; 1" monorepo/frontend/app/capacitor.config.ts > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/capacitor.config.ts

# cordova config
xmlstarlet edit -L --update "/widget/@id" --value  "$APP_ID" monorepo/frontend/app/config.xml
xmlstarlet edit -L --update "/widget/@version" --value "$APP_VERSION" monorepo/frontend/app/config.xml
xmlstarlet edit -L -N x="http://www.w3.org/ns/widgets" --update "//x:name" --value "$APP_NAME" monorepo/frontend/app/config.xml

SHORT_EXISTS=$(xmlstarlet sel -N x='http://www.w3.org/ns/widgets' -T -t -v '//x:name[@short]' monorepo/frontend/app/config.xml)

if [ -z "${SHORT_EXISTS:-}" ]; then
  #insert
  xmlstarlet edit -L -N x="http://www.w3.org/ns/widgets" -s "//x:name" -t attr -n "short" -v "$APP_DISPLAY_NAME" monorepo/frontend/app/config.xml
else
  #update
  xmlstarlet edit -L -N x="http://www.w3.org/ns/widgets" --update "//x:name[@short]" -v "$APP_DISPLAY_NAME" monorepo/frontend/app/config.xml
fi

# environment config
awk "/backend_url:.*,/ && !done { gsub(/backend_url:.*,/, \"backend_url: '$BACKEND_URL',\"); done=1}; 1" monorepo/frontend/app/src/environments/environment.production.ts > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/src/environments/environment.production.ts
awk "/backend_version:.*,/ && !done { gsub(/backend_version:.*,/, \"backend_version: '$BACKEND_VERSION',\"); done=1}; 1" monorepo/frontend/app/src/environments/environment.production.ts > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/src/environments/environment.production.ts
awk "/app_host:.*,/ && !done { gsub(/app_host:.*,/, \"app_host: '$APP_LINK_HOST',\"); done=1}; 1" monorepo/frontend/app/src/environments/environment.production.ts > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/src/environments/environment.production.ts
awk "/custom_url_scheme:.*,/ && !done { gsub(/custom_url_scheme:.*,/, \"custom_url_scheme: '$APP_URL_SCHEME',\"); done=1}; 1" monorepo/frontend/app/src/environments/environment.production.ts > tmp.$$.json && mv tmp.$$.json monorepo/frontend/app/src/environments/environment.production.ts
