#!/usr/bin/env sh

. $PWD/app.conf

ENTITLEMENTS_FILE="monorepo/frontend/app/ios/App/App/App.entitlements"
INFOPLIST_FILE="monorepo/frontend/app/ios/App/App/Info.plist"


/usr/libexec/PlistBuddy -c "Delete :com.apple.developer.associated-domains string applinks:$APP_LINK_HOST" $ENTITLEMENTS_FILE
/usr/libexec/PlistBuddy -c "Add :com.apple.developer.associated-domains array" $ENTITLEMENTS_FILE 
/usr/libexec/PlistBuddy -c "Add :com.apple.developer.associated-domains:0 string applinks:$APP_LINK_HOST" $ENTITLEMENTS_FILE

/usr/libexec/PlistBuddy -c "Delete :BGTaskSchedulerPermittedIdentifiers string com.transistorsoft.fetch" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :BGTaskSchedulerPermittedIdentifiers array" $INFOPLIST_FILE 
/usr/libexec/PlistBuddy -c "Add :BGTaskSchedulerPermittedIdentifiers:0 string com.transistorsoft.fetch" $INFOPLIST_FILE

/usr/libexec/PlistBuddy -c "Delete :UISupportedInterfaceOrientations~iphone array" $INFOPLIST_FILE

/usr/libexec/PlistBuddy -c "Delete :UISupportedInterfaceOrientations array" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations array" $INFOPLIST_FILE 
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations:0 string \"UIInterfaceOrientationPortrait\"" $INFOPLIST_FILE

/usr/libexec/PlistBuddy -c "Delete :UISupportedInterfaceOrientations~ipad array" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations~ipad array" $INFOPLIST_FILE 
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations~ipad:0 string \"UIInterfaceOrientationPortrait\"" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations~ipad:1 string \"UIInterfaceOrientationPortraitUpsideDown\"" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations~ipad:2 string \"UIInterfaceOrientationLandscapeLeft\"" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :UISupportedInterfaceOrientations~ipad:3 string \"UIInterfaceOrientationLandscapeRight\"" $INFOPLIST_FILE

/usr/libexec/PlistBuddy -c "Delete :UIViewControllerBasedStatusBarAppearance bool" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Add :UIViewControllerBasedStatusBarAppearance bool" $INFOPLIST_FILE
/usr/libexec/PlistBuddy -c "Set :UIViewControllerBasedStatusBarAppearance NO" $INFOPLIST_FILE

/usr/libexec/PlistBuddy -c "Delete :UIStatusBarStyle string" $INFOPLIST_FILE 
/usr/libexec/PlistBuddy -c "Add :UIStatusBarStyle string" $INFOPLIST_FILE 
/usr/libexec/PlistBuddy -c "Set :UIStatusBarStyle UIStatusBarStyleLightContent" $INFOPLIST_FILE


git -C monorepo checkout -- frontend/app/ios/App/App/AppDelegate.swift
