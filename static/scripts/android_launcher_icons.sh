#!/usr/bin/env sh

if [ "$1" = "clean" ] && [ -n "$2" ]; then
  find "$2/frontend/app/android/app/src/main" -name 'ic_launcher*' -delete
elif [ "$1" = "copy" ] && [ -n "$2" ] && [ -n "$3" ]; then
  find "$3/customizable/android" -name 'ic_launcher*' -delete
  cd "$2/frontend/app/android/app/src/main" || exit
  find . -name 'ic_launcher*' -exec cp --parents \{\} "$3/customizable/android" \;
else
  echo "Usage: "
  echo "  android_launcher_icons.sh clean [monorepo-path]"
  echo "  android_launcher_icons.sh copy [monorepo-path] [app-release-template-path]"
fi
