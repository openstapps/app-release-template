#!/usr/bin/env sh

# get semantical versioning string linke 2.0.0 (if $1 is v2.0.0) or $1
DEFAULT_VERSION="0.0.0"
APP_VERSION="${APP_VERSION:-$DEFAULT_VERSION}"

if echo -n $1 | grep -Eq 'v[0-9]+\.[0-9]+\.[0-9]+'; then 
  APP_VERSION=$(echo -n "$1" | cut -c 2-);
else 
  APP_VERSION=$1;
fi

if [ "$APP_VERSION" = "0.0.0" ]; then
  echo "Unsupported app version was set!"
  return 1
fi

git clone --depth 1 --branch $APP_VERSION https://gitlab.com/openstapps/openstapps.git monorepo
