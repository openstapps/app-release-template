SHELL := /bin/bash
APP_DIR := $(PWD)/app
BRANCH ?= main
START_TIME := $(date +%s)

clean:
	rm -rf monorepo

clone: clean
	sh static/scripts/clone_app.sh ${BRANCH}

install: clone
	cd monorepo && NG_CLI_ANALYTICS="false" pnpm install && pnpm turbo run build --filter=app
	cd monorepo/frontend/app && pnpm cap telemetry off

assets: install
	cp -rf customizable/assets/. monorepo/frontend/app/src/assets/ && cp -rf customizable/ios/. monorepo/frontend/app/resources/

configuration-web: assets
	sh static/scripts/ionic.sh

configuration-android: assets
	CONFIG_MODE=ANDROID sh static/scripts/ionic.sh

configuration-ios: assets
	CONFIG_MODE=IOS sh static/scripts/ionic.sh

web-build: configuration-web
	cd monorepo/frontend/app && ionic build --prod

web: web-build
	cd monorepo && pnpm turbo run build --filter=app
	mkdir -p monorepo/frontend/app/www/.well-known && source app.conf && sh static/scripts/universal_link_files.sh
	cd monorepo/frontend/app/www && zip -r ../../../../www.zip .
	echo "Web application artifact for version ${VERSION} is archived in www.zip"

prepare-android: configuration-android
	source app.conf && cd monorepo/frontend/app && rm -rf android && pnpm cap add android \
           && find android/app/src/main/res \( -name 'splash.*' -o -name 'ic_launcher*' \) -delete && cp -rf ../../../customizable/android/. android/app/src/main/ \
           && ionic capacitor build android --no-open --prod && cd ../../.. && sh static/scripts/android.sh
	cp -rf static/fastlane-android/. monorepo/frontend/app/android/fastlane/ && cp -rf app.conf monorepo/frontend/app/android/.env

android: prepare-android
	cd monorepo/frontend/app/android && bundler exec fastlane android release

android-open-beta: prepare-android
	cd monorepo/frontend/app/android && bundler exec fastlane android open_beta

android-internal-beta: prepare-android
	cd monorepo/frontend/app/android && bundler exec fastlane android internal_beta

prepare-ios: configuration-ios
	source app.conf && cd monorepo/frontend/app && rm -rf ios www && npx cap add ios && pnpm run resources:ios && pnpm ionic capacitor build ios --no-open --prod  && cd ../../.. && sh static/scripts/ios.sh
	cp -rf static/fastlane-ios/. monorepo/frontend/app/ios/App/fastlane/ && cp -rf app.conf monorepo/frontend/app/ios/App/fastlane/.env

ios: prepare-ios
	cd monorepo/frontend/app/ios/App && bundler exec fastlane ios release

ios-beta: prepare-ios
	cd monorepo/frontend/app/ios/App && bundler exec fastlane ios beta
