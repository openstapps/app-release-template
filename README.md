# App Release Template (ART)

This project can be used to generate mobile device apps (Android / iOS) and the standalone Angular web app. Beware that this is only the app/frontend part and your need a fully functioning backend deployment first. All iOS related workflows have to be run on an macOS device.

## Requirements

* A fully functioning and publicly accessible [backend](https://gitlab.com/openstapps/backend) deployment
* An active Apple App Store Developer Account and/or Google Playstore Developer Account
* Docker (Android and Angular builds)
* MacOS Device with latest Xcode, fastlane and xmlstarlet, node v18 and npm package @ionic/cli (iOS builds only)

## Docker

Use weelkly updated image from this repo

```bash
docker pull registry.gitlab.com/openstapps/app-release-template:latest
```

Or build your own image from Dockerfile within this project

```bash
docker build -t openstapps-art .
```

## Usage

0. If neccessary make adjustments to your app / profiles / entitlements in your corresponding developer/store portal
1. Overwrite assets in customizable directory with your own ones (keeping the same size and file format)
2. Edit app.conf.sample to your liking and rename it to app.conf (needs info you can find in your corresponding developer/store portal)
3. Use docker to run `make web` and `make android` (i.e. `docker run -it -v $(pwd):/build --rm openstapps-art:latest make web`)
4. On a macOS device run `make ios` (make sure you have all required certificates / profiles set up in Xcode)
5. Gitlab CI runs need to be provided with SCP targets and Base64 encoded SSH private keys via env variables for web and (optionally) apk deployments

### Splash & Icon assets

#### iOS

Replace the `logo.svg`

#### Android

Due to some newer quirks (Splash Screen API, adaptive icons, monochrome icons, vector assets, ...) Android asset generation doesn't work particularly
well using `capacitor-assets`.

The easiest way is to just use the [Image Asset Studio](https://developer.android.com/studio/write/create-app-icons#about)
included in Android Studio to generate all icons from your logo file.
This method cannot be automated, but just works.

Inside `frontend/app/android` in Android Studio:

1. Remove the sample icons
```shell
static/scrips/android_launcher_icons.sh clean [monorepo-path]
```
2. Right-click on the `res` folder, "New" => "Image Asset"
3. Set the name to `ic_launcher`
4. Select "Launcher Icons (Adaptive & Legacy)"
5. Select & crop your logo as the foreground layer (SVG preferred)
6. Select your background layer (color preferred)
7. In options, set all to "yes" and prefer webp
8. Click next & finish
To add monochrome support to your icons
9. As an optional step, add this line to `res/mipmap-anydpi-v26/ic_launcher[_round].xml`
   to enable monochrome icon support:
```xml
    <monochrome android:drawable="@drawable/ic_launcher_foreground" />
```
10. Copy the new icons into the release template
```shell
static/scrips/android_launcher_icons.sh copy [monorepo-path] [app-release-template-path]
```

Additional assets for splash screens are not required.
