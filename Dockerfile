FROM registry.gitlab.com/openstapps/openstapps/app-builder

ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    xmlstarlet \
    ruby-full \
    zip \
    openssh-client

RUN gem install bundler -v 2.4.22

RUN corepack enable && corepack prepare pnpm@latest-8 --activate

COPY . /build
WORKDIR /build

RUN bundler update
